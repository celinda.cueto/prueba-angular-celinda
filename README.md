# prueba-angular-Celinda Cueto

```
Aspectos Técnicos:
o Usar Angular 8 o mayor versión.
o Usar componentes, servicios, observadores, angular material.
o Una buena organización y estructura del proyecto serán tomadas en cuenta.
o Una buena maquetación de templates serán tomadas en cuenta.
o Buena práctica en el código serán tomadas en cuenta.

```

##Solucion

##Conceptos manejados

```
-Estructuracion de codigo, el codigo esta organizado por Componentes, Servicios e Interfaces
-Inyección de dependecias
-Observadores.

```

1. Se crean los componentes y servicios necesarios para el consumo de la Api y posteriormente proceder a la manipulacion de la informacion

##Creacion de componente

```
Se creó el componente Posts para manejar el frontend correspondiente a la muestra de la informacion proveniente de la api
"https://jsonplaceholder.typicode.com/posts"

```

##Creacion de servicio

```
Se creo el servicio PostServices para realizar la peticion de la api que se utlizó para obtener informacion sobre los posts,
 donde se creó la funcion cargar que realiza la peticion .


```
##Creación de Pipe


```
El pipe de angular permite manipular la informacion , por lo tanto se utlizo para generar el filtro de los posts 

```

##Estilos Frontend

```
La prueba requeria utlizar angular Material, sin embargo en la solucion se ultizo boostrap para la parte de Estilos 
por cuestion expereiencia en el manejo de este.
 Cabe resaltar que el proyecto se puede modificar de modo que se haga uso de Material.
 Si asi lo desea el evaluador en caso que sea excluyente.


```

