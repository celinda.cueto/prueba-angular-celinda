import { Component, OnInit } from '@angular/core';
import { PostService } from 'src/app/services/post.service';
import { PostInterface } from 'src/app/interfaces/post-interface';
@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
public posts:any=[]

filterPost=''
  constructor(private  postService:  PostService) {
   
   }
  ngOnInit(): void {
    this.postService.cargar()
      .subscribe(resp=>{
        console.log("respuesta",resp)
        this.posts=resp
      });
    
  }

}


