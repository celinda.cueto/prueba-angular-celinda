import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtro'
})
export class FiltroPipe implements PipeTransform {

  transform(value: any,arg:any ):any {
   
   const resultados=[]

   for(const post of value){
if(post.title.indexOf(arg)> -1){

  resultados.push(post)
}

   }
   
   return resultados;
  }

}
